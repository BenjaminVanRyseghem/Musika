#include "Arduino.h"

#include "play_sd_flac.h"
#include "play_sd_aac.h"
#include "codecs.h"
#include "audiobuffer.h"
#include "play_sd_mp3.h"

#include "SD.h"
#include "SPI.h"
#include "SerialFlash.h"
#include "Bounce.h"
#include "Wire.h"

#include "helpers.h"

#define DEBOUNCE 15
#define NUMBER_OF_BUTTONS 8

AudioPlaySdMp3 playMp31;
AudioOutputI2S i2s1;
AudioConnection patchCord1(playMp31, 0, i2s1, 0);
AudioConnection patchCord2(playMp31, 1, i2s1, 1);
AudioControlSGTL5000 sgtl5000_1;

elapsedMillis blinkTime = 0;
int led = LOW;
const char *lastPlayingFile;
int leds[NUMBER_OF_BUTTONS] = {0, 1, 2, 3, 4, 5, 24, 25};
const char *songs[NUMBER_OF_BUTTONS] = {"01.mp3", "02.mp3", "03.mp3", "04.mp3", "05.mp3", "06.mp3", "07.mp3", "08.mp3"};

struct Button {
    Bounce button;
    bool state;
    const char *filename;
    int pin;
    int led;
};

struct Button initButton(int pin, const char *filename, int led) {
    struct Button button = {
            Bounce(pin, DEBOUNCE),
            false,
            filename,
            pin,
            led
    };

    Serial.print(filename);
    Serial.print(": ");
    Serial.println(button.pin);

    pinMode(pin, INPUT_PULLUP);
    pinMode(led, OUTPUT);

    return button;
}

void demo() {
    digitalWrite(0, HIGH);
    digitalWrite(4, HIGH);
    delay(200);
    digitalWrite(0, LOW);
    digitalWrite(4, LOW);
    digitalWrite(1, HIGH);
    digitalWrite(5, HIGH);
    delay(200);
    digitalWrite(1, LOW);
    digitalWrite(5, LOW);
    digitalWrite(2, HIGH);
    digitalWrite(24, HIGH);
    delay(200);
    digitalWrite(2, LOW);
    digitalWrite(24, LOW);
    digitalWrite(3, HIGH);
    digitalWrite(25, HIGH);
    delay(200);
    digitalWrite(3, LOW);
    digitalWrite(25, LOW);
    delay(200);
    digitalWrite(0, HIGH);
    digitalWrite(1, HIGH);
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(24, HIGH);
    digitalWrite(25, HIGH);
    delay(400);
    digitalWrite(0, LOW);
    digitalWrite(1, LOW);
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(24, LOW);
    digitalWrite(25, LOW);
    delay(200);
}

void setup() {
    pinMode(13, OUTPUT);

    AudioMemory(5);
    Serial.begin(115200);
    sgtl5000_1.enable();
    sgtl5000_1.volume(0.3);

    SPI.setMOSI(7);
    SPI.setSCK(14);
    if (!(SD.begin(10))) {
        while (1) {
            Serial.println("Unable to access the SD card");
            delay(500);
        }
    }
    demo();
}

void playFile(const char *filename) {
    Serial.print("Playing file: ");
    Serial.println(filename);

    lastPlayingFile = filename;
    playMp31.play(filename);
}

void stopMusic() {
    playMp31.stop();
}

void updateButton(struct Button *button) {
    if (button->button.fallingEdge()) {
        Serial.print("Button (pin ");
        Serial.print(button->pin);
        Serial.println(") Press");
        led = HIGH;
        button->state = true;
    }
    if (button->button.risingEdge()) {
        Serial.print("Button (pin ");
        Serial.print(button->pin);
        Serial.println(") Release");
        led = LOW;
        bool isNotCurrentlyPlaying = !playMp31.isPlaying();

        if (button->state) {
            stopMusic();
            if (strcmp(lastPlayingFile, button->filename) || isNotCurrentlyPlaying) {
                playFile(button->filename);
            } else {
                Serial.print("Stoping file: ");
                Serial.println(button->filename);
            }
        }

        button->state = false;
    }

    if (!strcmp(lastPlayingFile, button->filename) && playMp31.isPlaying()) {
        digitalWrite(button->led, HIGH);
    } else {
        digitalWrite(button->led, LOW);
    }
}

void loop(struct Button buttons[]) {
    int i = 0;

    for (i = 0; i < NUMBER_OF_BUTTONS; i++) {
        buttons[i].button.update();
    }

    for (i = 0; i < NUMBER_OF_BUTTONS; i++) {
        updateButton(&buttons[i]);
    }

    digitalWrite(13, led);

    int knob = analogRead(A1);
    float vol = (float) knob / 1024.0;
    sgtl5000_1.volume(vol);
}

extern "C" int main(void) {
    int i;
    struct Button *buttons = (struct Button *) malloc(NUMBER_OF_BUTTONS * sizeof(struct Button));

    for (i = 0; i < NUMBER_OF_BUTTONS; i++) {
        buttons[i] = initButton(26 + i, songs[i], leds[i]);
    }

    setup();
    while (1) {
        loop(buttons);
        yield();
    }
}
